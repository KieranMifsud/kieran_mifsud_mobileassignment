package com.example.mobileapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private BlurLockView blurLockView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        blurLockView = (BlurLockView)findViewById(R.id.blurLockView);
        blurLockView.setBlurredView(imageView);

        imageView = (ImageView)findViewById(R.id.background);

        blurLockView.setCorrectPassword("7777");
        blurLockView.setLeftButton("");
        blurLockView.setRightButton("");

        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER,false);


        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {
                Toast.makeText(MainActivity.this, "Password correct", Toast.LENGTH_SHORT).show();
                blurLockView.hide(1000, HideType.FADE_OUT, EaseType.EaseInBounce);

                opennavbar();
            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(MainActivity.this, "Password incorrect", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void input(String inputPassword) {
            //
            }
        });

    }



    //open navbar activity
    public void opennavbar()
    {

        Intent intent = new Intent(this, navbar.class);
        startActivity(intent);

    }
}
